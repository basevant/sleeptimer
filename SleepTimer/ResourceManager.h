#pragma once

class CResourceManager
{
public:
	static const CString LoadStringFromResource(
		const UINT stringID
		) throw();
};
